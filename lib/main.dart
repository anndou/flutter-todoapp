import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'edit.dart';
import 'provider.dart';

void main() {
  runApp(ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Todo App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
          appBar: AppBar(title: const Text('Todo App')),
          body: const TodoListPage()),
    );
  }
}

class TodoListPage extends ConsumerWidget {
  const TodoListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final list = ref.watch(listProvider);
    return Scaffold(
      body: ListView.builder(
          padding: const EdgeInsets.all(8),
          itemCount: list.length,
          itemBuilder: (BuildContext context, int index) {
            return Dismissible(
                key: ObjectKey(list[index]),
                onDismissed: (DismissDirection direction) {
                  ref.read(listProvider.notifier).removeItem(index);
                },
                background: Container(
                  color: Colors.red,
                ),
                child: ListTile(
                    title: Text(list[index]),
                    onTap: () async {
                      String text = await Navigator.of(context).push(
                          MaterialPageRoute(builder: (BuildContext context) {
                        return TodoEditPage(
                          content: list[index],
                        );
                      }));
                      ref.read(listProvider.notifier).updateItem(index, text);
                    }));
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          String text = await Navigator.of(context)
              .push(MaterialPageRoute(builder: (BuildContext context) {
            return TodoEditPage(
              content: '',
            );
          }));
          ref.read(listProvider.notifier).addItem(text);
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
