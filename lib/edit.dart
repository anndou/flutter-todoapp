import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'provider.dart';

class TodoEditPage extends ConsumerWidget {
  String content;
  TodoEditPage({Key? key, required String this.content}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final textController = TextEditingController(text: this.content);
    String operationText = 'EDIT';
    if (this.content == '') {
      operationText = 'ADD';
    }
    return Scaffold(
        appBar: AppBar(
          title: Text(operationText + ' Todo'),
          automaticallyImplyLeading: false,
        ),
        body: Stack(children: <Widget>[
          Container(
            padding: const EdgeInsets.all(16),
            child: TextFormField(
                controller: textController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(16),
                  hintText: 'edit ...',
                )),
          ),
          Container(
              child: Center(
                  child: TextButton(
            onPressed: () {
              Navigator.of(context).pop(textController.text);
            },
            child: Text(operationText),
          )))
        ]));
  }
}
