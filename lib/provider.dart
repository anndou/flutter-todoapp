import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'data.dart';

// Provider定義
final listProvider =
    StateNotifierProvider<ListData, List<String>>((ref) => ListData());
