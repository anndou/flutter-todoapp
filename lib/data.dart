import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

// StateNotifier定義
class ListData extends StateNotifier<List<String>> {
  ListData() : super(['メモ１', 'メモ２', 'メモ３']);

  void addItem(String content) => state = [...state, content];

  void updateItem(int index, String content) {
    List<String> list = List.of(state);
    list[index] = content;
    state = list;
  }

  void removeItem(int index) {
    List<String> list = List.of(state);
    list.removeAt(index);
    state = list;
  }
}
